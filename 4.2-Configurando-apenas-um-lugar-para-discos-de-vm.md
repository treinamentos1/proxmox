Para não ocasionar erros, na escolha de onde adicionar o disco virtual na hora de criar uma VM, vamos deixar padrão somente o ponto de montagem **NFS**.

Datacenter   
--> Armazenamento   

Vá em todos os armazenamentos e em todos eles, remova a opção **Imagem de disco**.
