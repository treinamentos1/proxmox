Este repositório é um esboço para treinamento Proxmox atual versão 7.4

Fico feliz em ajudar à quem está precisando dos primeiros passos no Proxmox até a criação de VM's automatizadas no Terraform.

Você poderá ver o conteúdo no Mapa mental no link abaixo:
https://www.mindmeister.com/map/2721066298

![](mapa-mental.png)
