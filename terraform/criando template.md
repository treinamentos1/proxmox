**Para instalar algo nas imagens qcow2**
```bash
virt-edit imagem.qcow2 /etc/cloud/cloud.cfg
```

## Baixando as imagens

```bash
## Centos
#Stream 8
wget "https://cloud.centos.org/centos/8-stream/x86_64/images/CentOS-Stream-GenericCloud-8-20220913.0.x86_64.qcow2"
#Stream 9
wget "https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-GenericCloud-9-20230501.0.x86_64.qcow2"
#Centos 7
wget "https://cloud.centos.org/centos/7/images/CentOS-7-x86_64-GenericCloud.qcow2"

## Debian
#Buster (10)
wget "https://cloud.debian.org/images/cloud/buster/latest/debian-10-genericcloud-amd64.qcow2"
#Bullseye (11)
wget "https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-genericcloud-amd64.qcow2"
#Bookworm (12 dailies - not yet released)
wget "https://cloud.debian.org/images/cloud/bookworm/daily/latest/debian-12-genericcloud-amd64-daily.qcow2"

## Ubuntu
#20.04 (Focal Fossa)
wget "https://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64.img"
#22.04 (Jammy Jellyfish)
wget "https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-amd64.img"
#23.04 (Lunar Lobster) - daily builds
```

## Instalando agente Qemu e criando template
`vim criar-template.sh`
```shell
#!/bin/bash

# Dependencia
apt-get install -y libguestfs-tools 

# Defina o conjunto de linhas
vhdsOriginais=(
        "CentOS-7-x86_64-GenericCloud.qcow2" \
        "CentOS-Stream-GenericCloud-8-20220913.0.x86_64.qcow2" \
        "CentOS-Stream-GenericCloud-9-20230501.0.x86_64.qcow2" \
        "debian-10-genericcloud-amd64.qcow2" \
        "debian-11-genericcloud-amd64.qcow2" \
        "debian-12-genericcloud-amd64-daily.qcow2" \
        "ubuntu-20.04-server-cloudimg-amd64.img" \
        "ubuntu-22.04-server-cloudimg-amd64.img" \
        "lunar-server-cloudimg-amd64.img"
        )


nomesTemplate=(
              "template-centos-7" \
              "template-centos-8" \
              "template-centos-9" \
              "template-debian-10" \
              "template-debian-11" \
              "template-debian-12" \
              "template-ubuntu-20" \
              "template-ubuntu-22" \
              "template-ubuntu-23"
              )

loopnumero=0

# Loop para imprimir cada linha
for template in "${nomesTemplate[@]}"
do
  echo "Instalando agente no VHD "${vhdsOriginais[loopnumero]}""
  sleep 2
  virt-customize -a "${vhdsOriginais[loopnumero]}" --install 'qemu-guest-agent' --run-command 'systemctl enable qemu-guest-agent'


  echo "Criando Template template-$template-100$loopnumero"
  qm create 100$loopnumero --name "${nomesTemplate[loopnumero]}" --net0 virtio,bridge=vmbr0 && \
  qm importdisk 100$loopnumero "${vhdsOriginais[loopnumero]}" local-lvm && \
  qm set 100$loopnumero --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-100$loopnumero-disk-0 && \
  qm set 100$loopnumero --ide2 local-lvm:cloudinit && \
  qm set 100$loopnumero --boot c --bootdisk scsi0 && \
  qm set 100$loopnumero --serial0 socket --vga serial0 && \
  qm set 100$loopnumero --searchdomain cit.local && \
  qm set 100$loopnumero --nameserver 172.20.0.2 && \
  qm set 100$loopnumero --agent enabled=1 && \
  qm template 100$loopnumero

 
  loopnumero=$(($loopnumero+1))
done
```

## Criando os templates
```shell
qm create 1000 --name template-centos-7 --net0 virtio,bridge=vmbr0 && \
qm importdisk 1000 CentOS-7-x86_64-GenericCloud.qcow2 local-lvm && \
qm set 1000 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-1000-disk-0 && \
qm set 1000 --ide2 local-lvm:cloudinit && \
qm set 1000 --boot c --bootdisk scsi0 && \
qm set 1000 --serial0 socket --vga serial0 && \
qm set 1000 --searchdomain cit.local && \
qm set 1000 --nameserver 172.20.0.2 && \
qm set 1000 --agent enabled=1 && \
qm template 1000

qm create 1001 --name template-centos-8 --net0 virtio,bridge=vmbr0 && \
qm importdisk 1001 CentOS-Stream-GenericCloud-8-20220913.0.x86_64.qcow2 local-lvm && \
qm set 1001 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-1001-disk-0 && \
qm set 1001 --ide2 local-lvm:cloudinit && \
qm set 1001 --boot c --bootdisk scsi0 && \
qm set 1001 --serial0 socket --vga serial0 && \
qm set 1001 --searchdomain cit.local && \
qm set 1001 --nameserver 172.20.0.2 && \
qm set 1001 --agent enabled=1 && \
qm template 1001

qm create 1002 --name template-centos-9 --net0 virtio,bridge=vmbr0 && \
qm importdisk 1002 CentOS-Stream-GenericCloud-9-20230501.0.x86_64.qcow2 local-lvm && \
qm set 1002 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-1002-disk-0 && \
qm set 1002 --ide2 local-lvm:cloudinit && \
qm set 1002 --boot c --bootdisk scsi0 && \
qm set 1002 --serial0 socket --vga serial0 && \
qm set 1002 --searchdomain cit.local && \
qm set 1002 --nameserver 172.20.0.2 && \
qm set 1002 --agent enabled=1 && \
qm template 1002

qm create 1003 --name template-debian-10 --net0 virtio,bridge=vmbr0 && \
qm importdisk 1003 debian-10-genericcloud-amd64.qcow2 local-lvm && \
qm set 1003 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-1003-disk-0 && \
qm set 1003 --ide2 local-lvm:cloudinit && \
qm set 1003 --boot c --bootdisk scsi0 && \
qm set 1003 --serial0 socket --vga serial0 && \
qm set 1003 --searchdomain cit.local && \
qm set 1003 --nameserver 172.20.0.2 && \
qm set 1003 --agent enabled=1 && \
qm template 1003

qm create 1004 --name template-debian-11 --net0 virtio,bridge=vmbr0 && \
qm importdisk 1004 debian-11-genericcloud-amd64.qcow2 local-lvm && \
qm set 1004 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-1004-disk-0 && \
qm set 1004 --ide2 local-lvm:cloudinit && \
qm set 1004 --boot c --bootdisk scsi0 && \
qm set 1004 --serial0 socket --vga serial0 && \
qm set 1004 --searchdomain cit.local && \
qm set 1004 --nameserver 172.20.0.2 && \
qm set 1004 --agent enabled=1 && \
qm template 1004

qm create 1005 --name template-debian-11 --net0 virtio,bridge=vmbr0 && \
qm importdisk 1005 debian-12-genericcloud-amd64-daily.qcow2 local-lvm && \
qm set 1005 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-1005-disk-0 && \
qm set 1005 --ide2 local-lvm:cloudinit && \
qm set 1005 --boot c --bootdisk scsi0 && \
qm set 1005 --serial0 socket --vga serial0 && \
qm set 1005 --searchdomain cit.local && \
qm set 1005 --nameserver 172.20.0.2 && \
qm set 1005 --agent enabled=1 && \
qm template 1005

qm create 1006 --name template-ubuntu-20 --net0 virtio,bridge=vmbr0 && \
qm importdisk 1006 ubuntu-20.04-server-cloudimg-amd64.img local-lvm && \
qm set 1006 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-1006-disk-0 && \
qm set 1006 --ide2 local-lvm:cloudinit && \
qm set 1006 --boot c --bootdisk scsi0 && \
qm set 1006 --serial0 socket --vga serial0 && \
qm set 1006 --searchdomain cit.local && \
qm set 1006 --nameserver 172.20.0.2 && \
qm set 1006 --agent enabled=1 && \
qm template 1006

qm create 1008 --name template-ubuntu-23 --net0 virtio,bridge=vmbr0 && \
qm importdisk 1008 lunar-server-cloudimg-amd64.img local-lvm && \
qm set 1008 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-1008-disk-0 && \
qm set 1008 --ide2 local-lvm:cloudinit && \
qm set 1008 --boot c --bootdisk scsi0 && \
qm set 1008 --serial0 socket --vga serial0 && \
qm set 1008 --searchdomain cit.local && \
qm set 1008 --nameserver 172.20.0.2 && \
qm set 1008 --agent enabled=1 && \
qm template 1008
```


## Rascunhos

```bash
qm create 8000 --name template-centos-8 --memory 4096 --cores 2 --net0 virtio,bridge=vmbr0
qm set 8000 --scsihw virtio-scsi-pci --scsi0 local-lvm:0,import-from="/root/CentOS-Stream-GenericCloud-8-20220913.0.x86_64.qcow2",discard=on

qm set 8000 --boot order=scsi0 --scsihw virtio-scsi-single --agent enabled=1,fstrim_cloned_disks=1 --ide2 local-lvm:cloudinit

qm template 8000
```
####################


**Instalar o arquivo importante para converter a imagem qcow2 para img**
```bash
apt-get install qemu-utils
```

**Converter a imagem**
```bash
qemu-img convert -O raw debian-12-genericcloud-amd64-daily.qcow2 debian-12-genericcloud-amd64-daily.img
```

**Instalar o agente do qemu e outras aplicações**
```bash
virt-customize -a debian-12-genericcloud-amd64-daily.img --install 'qemu-guest-agent'
virt-customize -a debian-12-genericcloud-amd64-daily.img --run-command 'systemctl enable qemu-guest-agent'
virt-customize -a debian-12-genericcloud-amd64-daily.img --install 'vim'
```

# Fedora

wget https://edgeuno-bog2.mm.fcix.net/fedora/linux/releases/37/Cloud/x86_64/images/Fedora-Cloud-Base-37-1.7.x86_64.qcow2

**Converter a imagem**
```bash
qemu-img convert -O raw Fedora-Cloud-Base-37-1.7.x86_64.qcow2 Fedora-Cloud-Base-37-1.7.x86_64.img
```

**Instalar o agente do qemu e outras aplicações**
```bash
virt-customize -a Fedora-Cloud-Base-37-1.7.x86_64.qcow2 --install 'qemu-guest-agent'
virt-customize -a Fedora-Cloud-Base-37-1.7.x86_64.qcow2 --run-command 'systemctl enable qemu-guest-agent'
virt-customize -a Fedora-Cloud-Base-37-1.7.x86_64.qcow2 --install 'vim'

```
