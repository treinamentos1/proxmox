terraform {
  required_providers {
    proxmox = {
      source = "telmate/proxmox"
      version = ">= 2.9.14"
    }
  }
}

provider "proxmox" {
  pm_api_url = "https://192.168.1.200:8006/api2/json"
  pm_user = "root@pam"
  pm_password = "123456"
  pm_tls_insecure = true
}

resource "proxmox_vm_qemu" "cria-vm" {
  agent = 1
  name = "dzsrv011"
  desc = "Automacao de deploy com terraform"
  tags = "dev"
  target_node = "duzeru"
  onboot = true
  
  clone = "clonewin22"
  cores = 3
  sockets = 1
  memory = 6144
  scsihw = "virtio-scsi-pci"
  bootdisk = "scsi0"
  ipconfig0 = "ip=192.168.1.201/24,gw=192.168.1.1"

#  disk {
#    slot = 0
#    size = "10G"
#    type = "scsi"
#    storage = "local-lvm"
#  }

#  os_type = "l26"
  ciuser = "Admin"
  cipassword = "Th081217"
#  sshkeys = <<EOF
#  ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC+DYHQlUEyR5rfZmEaSZArlbQracUKO1G2jMaoXhwE31z7OO7huHdhMOcTHKQvWqQLdCy6/JCBQ/M/w/ONxk0G4D+TXNKMqhknVOT+nIkXsv/4z314ZW2EMkCvUf4bdH23DoSCFEgxP81bsKMtIMQ6TbzKr7VS0XRXcaS1w1/7Iuxzx4jKOzlEpi1sfrquUeY5iqj+SFAuL27+1KnDeVfZYXXaAESSndndrX2LDkGuPszoy6ro3oilSbOvD30hIGqyHBvlZrBG45l9XWh4ZWiyACQd3+FfpolzGx99UNuCrri6GWl9kdutk4E1YwZQhodBq59T11yEOjAwBsRMDohJyaUslQP4MIdStEtIk0A6wLWTpfvR+13KYAJNFCdub0amcHn+VAwTP/jyy7D6eWxb7UArFgWS3+o6SobbBav9GRpYo6ar/QHznEh5PNh9/MN72cW2Mj0udJZebLgoQm87JxGnjIWSiw7h9vAVHRA9MDeGzLdgAgxOcUFPI8AJTaE= cadu@pc
#  EOF
}

#resource "null_resource" "exec_shell" {
#  connection {
#    type        = "ssh"
#    user        = "central"
#    private_key = file("~/.ssh/id_rsa")
#    host        = "192.168.1.201"
#  }
#
#  provisioner "remote-exec" {
#    inline = [
#      "echo 'Comando que você deseja executar' >> /tmp/meu_arquivo.txt",
#      "echo 'Outro comando que você deseja executar' >> /tmp/meu_arquivo.txt",
#      "sudo touch /opt/meu_arquivo.txt",
#    ]
#  }
#}
