## Migrar Centos

**Exportar a VM do Hyper-V**

**Executar o comando para converter a imagem:**
```shell
qemu-img convert -p -f vhdx -O qcow2 /original.vhdx /convertido.qcow2
```

**Criando e configurando uma VM**
DISCO="win2019.qcow2"
LOCAL="local-lvm"
VMID="100"
VMNAME="centos7"

```shell
**Criando o template**
qm create $VMID --name $VMNAME --net0 virtio,bridge=vmbr0
qm importdisk $VMID $DISCO $LOCAL
qm set $VMID --scsihw virtio-scsi-pci --ide0 $LOCAL:vm-$VMID-disk-0
qm set $VMID --boot c --bootdisk ide0
qm set $VMID --agent enabled=1
qm set $VMID --memory 4096 --cores 2
qm set $VMID --onboot 1
qm set $VMID --ostype l26 
qm set $VMID --tags "dev"
qm start $VMID
```

**Instalar o agente e habilitar para subir junto ao Sistema Operacional**
```shell
yum install -y qemu-guest-agent
systemctl start qemu-guest-agent
systemctl enable qemu-guest-agent
```

## Windows
