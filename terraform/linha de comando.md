# Etapa 1: Preparando a imagem

> Todos estes comandos devem ser executados com o root do Proxmox ou em outro servidor
**Baixar a imagem no servidor Proxmox**
```bash
wget https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img
wget https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img
```

**Instalar a ferramenta para mudanças na imagem**
```bash
apt update -y && apt install libguestfs-tools -y
```

**criar um usuário e adicionar uma chave ssh**
```bash
virt-customize -a jammy-server-cloudimg-amd64.img --run-command 'useradd central' && \
virt-customize -a jammy-server-cloudimg-amd64.img --run-command 'mkdir -p /home/central/.ssh' && \
virt-customize -a jammy-server-cloudimg-amd64.img --ssh-inject central:file:/home/cadu/.ssh/id_rsa.pub && \
virt-customize -a jammy-server-cloudimg-amd64.img --run-command 'chown -R central:central /home/central'
```

**Instalar o agente do qemu e outras aplicações**
```bash
virt-customize -a jammy-server-cloudimg-amd64.img --install 'qemu-guest-agent'
virt-customize -a jammy-server-cloudimg-amd64.img --install 'vim'
```

**Liberar o acesso de user com senha**
```bash
virt-customize -a jammy-server-cloudimg-amd64.img --run-command 'sed -i s"/PasswordAuthentication no/PasswordAuthentication yes/" /etc/ssh/sshd_config'
```

# Etapa 2
**Cria uma VM para o template**
```bash
qm create 9000 --name "ubuntu-2004-cloudinit-template" --memory 2048 --cores 2 --net0 virtio,bridge=vmbr0
```

**Importa o disco baixado para local-lvm/discos da vm, onde em seguida, voce pode ver este disco com o nome vm-9000-disk-0**
```bash
qm importdisk 9000 jammy-server-cloudimg-amd64.img local-lvm
```

**Insere o disco no template da VM que criamos anteriormente**
```bash
qm set 9000 --scsihw virtio-scsi-pci --scsi0 local-lvm:vm-9000-disk-0

qm set 9000 --boot c --bootdisk scsi0
qm set 9000 --ide2 local-lvm:cloudinit
qm set 9000 --serial0 socket --vga serial0
qm set 9000 --agent enabled=1
```

**Criar o template**
```bash
qm template 9000
```

# Etapa 3
**clone a VM (aqui estamos clonando o modelo com ID 9000 para uma nova VM com ID 999):**
```bash
qm clone 9000 999 --name test-clone-cloud-init
```

**Execute as opções abaixo**
```bash
qm set 999 \
  --sshkey ~/.ssh/vm.pub \
  --ipconfig0 ip=192.168.1.201/24,gw=192.168.1.1 \
  --description "Eis o teste de criação de VM na linha de comando bem-sucedido" \
  --tags "dev" \
  --keyboard pt-br \
  --cores 3 \
  --memory 6144 \
  --onboot 1 \
  --ostype l26 \
  --resize +10G
```

**Expandir o disco**
```bash
qm resize 999 scsi0 +20G
```

**Iniciar a VM clonada**
```bash
qm start 999
```

# Comando uteis
**Usuário e senha**
--cipassword
-ciuser

**Expandir o tamanho do disco**
qm resize 999 scsi0 +5G

**iniciar a VM clonada**
qm start 999

**Lista as VMs**
qm list

# Fonte:
- https://pve.proxmox.com/pve-docs/qm.1.html
