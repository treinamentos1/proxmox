## Baixar templates

### Debian
**Buster (10)**   
```wget "https://cloud.debian.org/images/cloud/buster/latest/debian-10-genericcloud-amd64.qcow2"```

**Bullseye (11)**   
```wget "https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-genericcloud-amd64.qcow2"```

**Bookworm (12 dailies - not yet released)**   
```wget "https://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64.img"```

### Ubuntu
**20.04 (Focal Fossa)**     
```wget "https://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64.img"```
 
**22.04 (Jammy Jellyfish)**   
```wget "https://cloud-images.ubuntu.com/releases/22.04/release/ubuntu-22.04-server-cloudimg-amd64.img"```

**23.04 (Lunar Lobster) - daily builds**   
```wget "https://cloud-images.ubuntu.com/lunar/current/lunar-server-cloudimg-amd64.img"```

### CentOS Stream
**Stream 8**   
```wget https://cloud.centos.org/centos/8-stream/x86_64/images/CentOS-Stream-GenericCloud-8-20220913.0.x86_64.qcow2```

**Stream 9 (daily) - they don't have a 'latest'**   
```wget https://cloud.centos.org/centos/9-stream/x86_64/images/CentOS-Stream-GenericCloud-9-20230123.0.x86_64.qcow2```
