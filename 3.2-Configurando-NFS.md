Requisitos
- Sistema Operacional Debian
- 1G de RAM
- 1 vCPU
- 64G de disco
- Rede em modo Bridge

Vamos baixar a imagem do Debian Minimal para o sevidor de NFS
- https://www.debian.org/CD/netinst/

## Servidor NFS

No Debian e derivados:
```
apt-get install -y vim ssh nfs-kernel-server
```

Criar diretório NFS raiz para compartilhamento:
```
mkdir -p /storage/bkp && \
chown nobody:nogroup /storage/bkp && \
chmod 777 /storage/bkp
```

Definir acesso para clientes NFS no arquivo de exportação:
```
vim /etc/exports

/storage/bkp  IP_AQUI(rw,sync,no_root_squash,no_subtree_check)
```

Disponibilize o compartilhamento NFS para o cliente
```
systemctl restart nfs-kernel-server && systemctl enable nfs-kernel-server
```

Vamos adicionar um IP estático no Debian, acesse o arquivo abaixo e insira as linhas:
`vim /etc/networks/interface`
```config
iface enp0s3 inet static

address 192.168.1.60
netmask 255.255.255.0
gateway 192.168.1.1
```

Reinicie o servidor.

## Adicionando ao NFS

Agora que temos o NFS instalado e configurado, vá até o Proxmox
Datacenter    
--> Armazenamento

Clique em **Adicionar** e selecione a opção **NFS**

Preencha os campos da seguinte forma:
- **ID:** bkp-nfs
- **Servidor:** 192.168.1.60
- **Export:** /storage/bkp
- **Conteúdo:** Arquivo de backup VZDump
