Crie um diretório dentro do Proxmox

```shell
mkdir -p /storage/vhd
```

Acesse o Proxmox e navegue até   
Datacenter   
--> Armazenamento

Clique em **Adicionar e selecione  **Diretório**.   
- **ID:** vhd
- **DIretório:** /storage/vhd
- **Conteúdo:** Imagem de disco
- **Habilitado:** Deixe marcado

Clique para concluir.

Agora temos na lista o novo lugar para armazenarmos os novos VHD's das máquinas virtuais.
