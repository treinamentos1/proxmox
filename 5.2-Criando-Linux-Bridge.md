Vamos acessar:
Datacenter   
--> duzeru   
----> Sistema   
------> Rede

Vá até a placa de rede e clique duas vezes para editar.   
- Marque início automático.
- Clique em **Aplicar configuração**.

Clique em Criar:
- **Nome:** vmbr1
- **Ipv4/CIDR:** 10.10.10.1/24 
- **Portas da ponte:** O nome da interface de rede que foi ativada
- **Comentário:** Ponte de teste
- Clique em **Aplicar a configuração**

Para testar, podemos adicionar esta Linux Bridge na máquina Windows.
- Selecione a VM Windows
- Acesse Hardware
- Clique em **Adicionar**
- Selecione dispositivo de rede
- **Ponte:** vmbr1
- **Modelo:** VirtIO (paravirtualizado)
- Clique em **Adicionar**

Agora acesse o console da VM Windows e identifique a segunda placa de rede.

Atribua um IPV4 a ela:
- **IPAddress:** 10.10.10.2
- **Subnet:** 255.0.0.0
- **Gateway:** 10.10.10.1
- **DNS Server:** 8.8.8.8
- Verifique no Powershell com o comando `ipconfig` que já temos os dois IP's das duas placas de rede.
- Altere o nome da VM.
- Desative o Firewall
- Desligue-a.

